include windower.asm
KEY_ALT equ 1
KEY_DOWN equ 2
KEY_MODE equ 4
.const
lib_kbd_name db"kbd.dll",0
lib_kbd_name_hook_install db"installHook",0
lib_kbd_name_hook_remove db"uninstallHook",0
lib_kbd_name_hook_get_key_state db"getKeyState",0
table		db  	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	10,1,2,3,4,5,6,7,8,9,0,0,0,0,0,0
		db	0,33,53,51,35,19,36,37,38,24,39,40,41,55,54,25
		db	26,17,20,34,21,23,52,18,50,22,49,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,42,12,56,11,57,58
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,27,28,0,43,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		db	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 

 
.data
handle_of_comp dd 0
handle_of_dll dd 0
hook_install dd 0
hook_remove dd 0
hook_get_key_state dd 0
test_msg   db "test msg",0
.code
;===================================================================================
;kbd thread
;===================================================================================
kbd_thread proc lparam :DWORD
kbd_thread_start:
       ;push MB_OK
        ;push 0
        ;push 0
        ;call MessageBox
        call dword ptr hook_get_key_state
;-----------------check alt       
; 
        mov	bl,ah
        xor	bl,3;KEY_ALT+KEY_DOWN
	jnz		kbd_thread_1
	cmp		al,VK_NUMPAD0
	jl		kbd_thread_1
	cmp		al,VK_NUMPAD9
	jg		kbd_thread_1
	push		eax
	sub		al,VK_NUMPAD0
	xor		ebx,ebx
	mov		bl,al
        ;now in al window number
;        and we need to call some funct 
	invoke 	windower_enum,ebx
	pop eax
;       check for num0-9
;	call select window 
;-----------------check mode	        
kbd_thread_1:
	push eax
	mov bl,ah
	xor bl,KEY_MODE+KEY_DOWN    
	jnz kbd_thread_1_nokey
	;zbis
	mov ebx,0
	mov bl,al
	lea ecx,table
	add ecx,ebx
	mov ebx,[ecx]
	test		ebx,ebx
	jz		kbd_thread_1_nokey
	invoke windower_cmd,ebx
kbd_thread_1_nokey:	
	pop eax
kbd_thread_2:       
        invoke SleepEx,5,0
	jmp kbd_thread_start
	Ret
kbd_thread EndP
;===================================================================================
;open kbd
;===================================================================================
kbd_open proc
	invoke LoadLibrary,offset lib_kbd_name
	mov handle_of_dll, eax
	test eax,eax
	jz kbd_open_error 
	invoke GetProcAddress,handle_of_dll, offset lib_kbd_name_hook_install
	mov hook_install,eax 
	invoke GetProcAddress,handle_of_dll, offset lib_kbd_name_hook_remove
	mov hook_remove,eax
	invoke GetProcAddress,handle_of_dll, offset lib_kbd_name_hook_get_key_state
	mov hook_get_key_state,eax 
	
	mov eax,hook_install
	call eax
	;call dword ptr hook_install
	
	
	invoke CreateThread, NULL,0,addr kbd_thread,NULL,0,NULL
	
	  
	invoke	windower_open
	Ret
kbd_open_error:
	Ret	
kbd_open EndP
;===================================================================================
;close kbd
;===================================================================================
kbd_close proc
	cmp		handle_of_dll,0
	jne		kbd_close_1
	call	 	dword ptr hook_remove
	invoke 	FreeLibrary,handle_of_dll
	invoke	windower_close 
kbd_close_1:	
	Ret
kbd_close EndP


