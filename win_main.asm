
      include kbd.asm
;NIF_MESSAGE equ 0x00000001
;NIF_ICON equ 0x00000002
PP_MENUITEM_HEADERS		equ 1001
PP_MENUITEM_HELP			equ 1002
PP_MENUITEM_EXIT			equ 1003


.data
        oppa db "Template",0
        szDisplayName db "Template",0
        CommandLine   dd 0
        hWnd          dd 0
        hInstance     dd 0
        szClassName   db "Template_Class",0
        menu_text_headers   db "���������",0
        menu_text_help	    db "������",0
        menu_text_exit      db "�����",0

        hhmenu  dd 0
        hhmenu2  dd 0
        hmenu   dd 0
        hmenu2  dd 0
        szFile db 04,10, 0
        nid NOTIFYICONDATA <>
.code

win_main_create proc hInst     :DWORD,
             hPrevInst :DWORD,
             CmdLine   :DWORD,
             CmdShow   :DWORD
	

        ;====================
        ; Put LOCALs on stack
        ;====================
	;LOCAL nid   :NOTIFYICONDATA
         LOCAL wc   :WNDCLASSEX
                

        LOCAL Wwd  :DWORD
        LOCAL Wht  :DWORD
        LOCAL Wtx  :DWORD
        LOCAL Wty  :DWORD

        ;==================================================
        ; Fill WNDCLASSEX structure with required variables
        ;==================================================
        nop
	mov eax, sizeof HMENU
	nop

        mov wc.cbSize,         sizeof WNDCLASSEX
        mov wc.style,          CS_HREDRAW or CS_VREDRAW 
        ;\or CS_BYTEALIGNWINDOW
        mov wc.lpfnWndProc,    offset WndProc
        mov wc.cbClsExtra,     NULL
        mov wc.cbWndExtra,     NULL
        m2m wc.hInstance,      hInst
        mov wc.hbrBackground,  COLOR_BTNFACE+1
        mov wc.lpszMenuName,   NULL
        mov wc.lpszClassName,  offset szClassName

        ;push 500
        ;push hInst
        ;call LoadIcon
        ;mov wc.hIcon, eax
        mov wc.hIcon, 0
        
        push IDC_ARROW
        push NULL
        call LoadCursor
        mov wc.hCursor, eax
        mov wc.hIconSm, 0

        lea eax, wc
        push eax
        call RegisterClassEx
;-----------------------------------------------------------------------------------------
        mov Wwd, 500
        mov Wht, 350

        push SM_CXSCREEN
        call GetSystemMetrics
	xor eax,eax
	mov Wty,eax
	xor Wtx,eax
        push NULL
        push hInst
        push NULL
        push NULL
        push Wht
        push Wwd
        push Wty
        push Wtx
        push 0; WS_OVERLAPPEDWINDOW
        push offset szDisplayName
        push offset szClassName
        push 0;WS_EX_OVERLAPPEDWINDOW
        call CreateWindowEx

         mov   hWnd,eax

	mov nid.cbSize , sizeof NOTIFYICONDATA
	mov eax,hWnd
    	mov nid.hwnd, eax
    	mov nid.uID, 501
    	mov nid.uFlags, NIF_ICON;|NIF_MESSAGE
    	or nid.uFlags,NIF_MESSAGE

    	invoke LoadIcon,hInst,500
    	mov nid.hIcon,eax
   	mov nid.uCallbackMessage ,  WM_USER+1
	
	invoke Shell_NotifyIcon,NIM_ADD,addr nid

	call CreatePopupMenu
	mov hmenu,eax
	
	
	
 	
 	;invoke AppendMenu,hmenu,MF_STRING or MF_ENABLED,PP_MENUITEM_HEADERS,addr menu_text_headers
 	;invoke AppendMenu,hmenu,MF_STRING or MF_ENABLED,PP_MENUITEM_HELP,addr menu_text_help
 	invoke AppendMenu,hmenu,MF_STRING or MF_ENABLED,PP_MENUITEM_EXIT,addr menu_text_exit
	
	 
;        push SW_SHOWNORMAL
;       push hWnd
;       call ShowWindow
;
;       push hWnd
;       call UpdateWindow

	invoke kbd_open
	Ret
win_main_create EndP


; #########################################################################
win_main_destruct proc hWin     :DWORD
	invoke kbd_close
	invoke Shell_NotifyIcon,NIM_DELETE,addr nid
	invoke PostMessage,hWin,WM_QUIT,0,0
	Ret
win_main_destruct EndP

; #########################################################################
WndProc proc hWin   :DWORD,
             uMsg   :DWORD,
             wParam :DWORD,
             lParam :DWORD
             
             LOCAL coord   :POINT

    cmp uMsg, WM_COMMAND
    jne nxt1
      cmp wParam, 1000
      jne cmd1
        push NULL
        push SC_CLOSE
        push WM_SYSCOMMAND
        push hWin
        call SendMessage
;------------------------------------------------------------------------        
      cmd1:
      cmp wParam, 1900
      jne cmd2
        push MB_OK
        push offset szDisplayName   ; in .data section
;        push offset TheMsg          ; in .data section
        push hWin
        call MessageBox

      cmd2:
;------------------------------------------------------------------------
; WM_CLOSE
    nxt1:
    cmp uMsg, WM_CLOSE
    jne nxt2
      ;push MB_YESNO
      ;push offset szDisplayName     ; in .data section
      ;push offset TheText           ; in .data section
      ;push hWin
      ;call MessageBox
        ;cmp eax, IDNO
        ;jne nxt2
      push NULL
      call PostQuitMessage
        
       xor eax, eax              ; put zero in eax
       ret                       ; exit the proc
;------------------------------------------------------------------------
; WM_DESTROY
    nxt2:
      cmp uMsg, WM_DESTROY
      jne nxt3
      push NULL
      call PostQuitMessage
      xor eax, eax                  ; put zero in eax
      ret                           ; exit the proc
;------------------------------------------------------------------------
; WM_USER+1 message from trayicon
nxt3:
	cmp uMsg,WM_USER+1;from trayicon
	jne nxt4
	cmp lParam,WM_LBUTTONDOWN 
	je nxt3_1
	cmp lParam,WM_RBUTTONDOWN 
	je nxt3_1
	cmp lParam,WM_KILLFOCUS
	jne nxt3_end
	;invoke PostMessage,hWin,WM_QUIT,0,0
	jmp nxt3_end
	nxt3_1:
	invoke SetForegroundWindow,hWnd
	invoke GetCursorPos,addr coord
	invoke TrackPopupMenu,hmenu,TPM_LEFTALIGN or TPM_VERNEGANIMATION,coord.x,coord.y,0,hWin,0
	invoke		PostMessage,hWnd,WM_NULL,0,0
	nxt3_end:
	ret
;------------------------------------------------------------------------
; WM_COMMAND
nxt4:
	cmp uMsg,WM_COMMAND
	jne nxt999
	cmp wParam,PP_MENUITEM_HEADERS ; "headers" menu item
	jne nxt4_1
	;code
	ret
	nxt4_1:
	cmp wParam,PP_MENUITEM_HELP ; "help" menu item
	jne nxt4_2
	;code
	ret
	nxt4_2:
	
	
	cmp wParam,PP_MENUITEM_EXIT ; "headers" menu item
	jne nxt4_3
	;code
	;invoke PostMessage,hWin,WM_QUIT,0,0
	invoke win_main_destruct,hWin
	ret
	nxt4_3:
	ret
;------------------------------------------------------------------------
; default	
    nxt999:
      push lParam
      push wParam
      push uMsg
      push hWin
      call DefWindowProc

    ret

WndProc endp


