;include C:\masm32\include\windows.inc 
.const
.data
hwnds	dd 16 dup (0)
wndmsg	db "0",0,0,0
.code
;===================================================================================
;kbd thread
;===================================================================================
windower_thread proc lparam :DWORD
	xor		eax,eax
windower_thread_start:
	lea		ebx,hwnds
	add		ebx,eax
	add		ebx,eax
	add		ebx,eax
	add		ebx,eax
	push		eax
	mov		ecx,[ebx]
	test		ecx,ecx
	jz		windower_thread_nownd
	and		eax,0fh
	add		eax,0030h
	mov		wndmsg,al
	push		ecx
	invoke	SetWindowText,ecx,offset wndmsg
	
	test		eax,eax
	jnz		windower_thread_text_ok
	xor		eax,eax
	mov		[ebx],eax;error window lost
	windower_thread_text_ok:	
		
         invoke SleepEx,10,0
         pop		ecx
         pop 		eax
         push		eax
         cmp		eax,0
         jne		windower_thread_no_shots		
         invoke	PostMessage,ecx,WM_KEYDOWN,VK_F9,WM_KEYUP
         
windower_thread_no_shots:
windower_thread_nownd:
	pop 		eax
         inc		eax
         and		eax,15
	jmp windower_thread_start
	
	Ret
windower_thread EndP
;===================================================================================
;open kbd
;===================================================================================
windower_open proc
	invoke CreateThread, NULL,0,addr windower_thread,NULL,0,NULL
	Ret
windower_open_error:
	Ret	
windower_open EndP
;===================================================================================
;close kbd
;===================================================================================
windower_close proc
	
	Ret
windower_close EndP

;===================================================================================
;enum windows
;===================================================================================
windower_enum proc wnd_num :DWORD
	push		ebx
	;calc addr in hwnds storage
	lea		ebx,hwnds
	add		ebx,wnd_num
	add		ebx,wnd_num
	add		ebx,wnd_num
	add		ebx,wnd_num
	;get hwnd of top
	invoke	GetForegroundWindow
	;store 2 storage
	mov		[ebx],eax
	;				
	invoke Beep,2000,10
	pop		ebx
	Ret
windower_enum EndP
;===================================================================================
;commant 2  window
;===================================================================================
windower_cmd proc wnd_cmd :DWORD
	push		eax
	push		ebx
	push		ecx
	
	mov		ebx,wnd_cmd
	sar		ebx,4
	and		ebx,15
	add		ebx,1
	lea		ecx,hwnds
	add		ecx,ebx
	add		ecx,ebx
	add		ecx,ebx
	add		ecx,ebx
	mov		ebx,[ecx];in ebx hwnd
	test		ebx,ebx;check for NULL
	jz		windower_cmd_nownd		
	;calc	key
	mov		eax,wnd_cmd
	and		eax,15;low 4 bit
	add		eax,VK_F1;now key_code in eax
	dec		eax
	invoke	PostMessage,ebx,WM_KEYDOWN,eax,WM_KEYUP
	;invoke	PostMessage,ebx,WM_KEYDOWN,VK_F1,WM_KEYUP
windower_cmd_nownd:	
	pop		ecx
	pop		ebx
	pop		eax
	Ret
windower_cmd EndP
	