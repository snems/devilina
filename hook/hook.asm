.386

.model flat,stdcall

option casemap:none

include \masm32\include\windows.inc

include \masm32\include\kernel32.inc

includelib \masm32\lib\kernel32.lib

include \masm32\include\user32.inc

includelib \masm32\lib\user32.lib

KEY_ALT equ 1
KEY_DOWN equ 2
KEY_MODE equ 4

.const

WM_MOUSEHOOK equ WM_USER+6

.data



ts	dd 0
hInstance dd 0

clb db "callBack",0

.data?

hHook dd ?
hWnd dd ?
mutex db ?
keys dd 256 dup (?)
keys_cnt dd ?
mode db ?

.code
;===================================================================================
;
;===================================================================================
pushKeyFIFO proc new_item:DWORD
	push ebx
	cmp keys_cnt,255
	jge pushKeyFIFO_1
	lea eax, keys
	add eax, keys_cnt
	add eax, keys_cnt
	add eax, keys_cnt
	add eax, keys_cnt
	mov ebx,new_item
	mov [eax],ebx
	inc keys_cnt
pushKeyFIFO_1:	
	pop ebx
	mov eax,keys_cnt
	ret	
pushKeyFIFO endp
;===================================================================================
;
;===================================================================================
popKeyFIFO proc
	push ebx
	push ecx
	mov ebx,0
	mov eax,keys_cnt
	test eax,eax
	jz popKeyFIFO_exit;have one or more item

	mov ecx,keys_cnt
	add ecx,ecx
	add ecx,ecx
	lea ebx, keys
	add ecx,ebx
	mov eax, keys
popKeyFIFO_shift:
	push [ebx+4]
	pop [ebx+0]
	push [ebx+5]
	pop [ebx+1]
	push [ebx+6]
	pop [ebx+2]
	push [ebx+7]
	pop [ebx+3]

	add ebx,4
	cmp ecx,ebx
	jne popKeyFIFO_shift
	dec keys_cnt
popKeyFIFO_exit:
	
	pop ecx
	pop ebx

	ret	
popKeyFIFO endp
;===================================================================================
;
;===================================================================================
DllEntry proc hInst:HINSTANCE, reason:DWORD, reserved1:DWORD

		push hInst

		pop hInstance
;		mov mutex,0
;		mov hHook,0

      	mov  eax,TRUE
      	ret

DllEntry Endp

;===================================================================================
;lParam+0 vkCode
;lParam+4 scanCode
;lParam+8 flags
;lParam+12 time
;===================================================================================
callBack proc nCode:DWORD,wParam:DWORD,lParam:DWORD
	push ebx
	push ecx

callBack_1:
	mov al,mutex
	test al,al
	jz callBack_2
	invoke Sleep,3
	jmp callBack_1
callBack_2:
	inc	mutex 
	;load vkCode 2 cl
	mov eax,lParam
	mov ebx,[eax]
	mov cl,bl
	;load flags ...
	mov ch,0
	add eax,8
	mov ebx,[eax]		
	test ebx,LLKHF_ALTDOWN
	jz callBack_3
	or ch,KEY_ALT
	callBack_3:
	;if down set flag
	or ch,KEY_DOWN
	cmp wParam,WM_KEYDOWN
	je callBack_9
	cmp wParam,WM_SYSKEYDOWN
	je callBack_9
	xor ch,KEY_DOWN
	callBack_9:
	
	;cmp cl,VK_PAUSE ;if pressed
	cmp cl,VK_APPS ;if pressed
	jne callBack_8
	
	;if key down, set mode to true
	cmp wParam,WM_KEYDOWN
	je callBack_5
	cmp wParam,WM_SYSKEYDOWN
	je callBack_5
	jmp callBack_6
	callBack_5:
	mov mode,1
	callBack_6:
	;else, if key up, set mode to false
	cmp wParam,WM_KEYUP
	je callBack_7
	cmp wParam,WM_SYSKEYUP
	je callBack_7
	jmp callBack_8
	callBack_7:
	mov mode,0
	callBack_8:
	;set flag if mode
	cmp mode,1
	jne callBack_10
	or	ch,KEY_MODE
	callBack_10:
	;push 2 keystack
	invoke pushKeyFIFO,ecx	
	;if pause key pressed, blok it
	cmp mode,1
	je callBack_block_message
	callBack_4:	
;callBack_block_message:
	invoke CallNextHookEx,hHook,nCode,wParam,lParam
callBack_block_message:
	inc	ts
	mov	mutex,0
	pop ecx
	pop ebx
	ret	
callBack endp

;===================================================================================
;
;===================================================================================
installHook proc 

	;push hwnd

	;pop hWnd
;	mov eax,hwnd
;	mov hWnd,eax
	;invoke GetProcAddress,hInstance, clb
	
	invoke SetWindowsHookEx,WH_KEYBOARD_LL,addr callBack,hInstance,NULL

	mov hHook,eax
	mov eax,1
	mov mutex,0
	mov keys_cnt,0
	mov mode,0
	ret 

installHook endp

;===================================================================================
;
;===================================================================================
uninstallHook proc
uninstallHook_1:
	mov al,mutex
	test al,al
	jz uninstallHook_2
	invoke Sleep,3
	jmp uninstallHook_1
uninstallHook_2:
	inc	mutex 
	
	
	invoke UnhookWindowsHookEx,hHook

	mov	mutex,0
	ret
uninstallHook endp

;===================================================================================
;
;
;===================================================================================
getKeyState proc	
getKeyState_1:
	mov al,mutex
	test al,al
	jz getKeyState_2
	invoke Sleep,3
	jmp getKeyState_1
getKeyState_2:
	inc	mutex 

	

    call popKeyFIFO
;	mov keys,0

	mov	mutex,0
	ret
getKeyState endp




End DllEntry



