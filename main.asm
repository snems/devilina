; #########################################################################

      .586
      .model flat, stdcall
      option casemap :none   ; case sensitive

; #########################################################################

      include \masm32\include\windows.inc
      include \masm32\include\user32.inc
      include \masm32\include\kernel32.inc
      include shell32.inc

      includelib \masm32\lib\user32.lib
      includelib \masm32\lib\kernel32.lib
      includelib \masm32\lib\shell32.lib
      


; #########################################################################

      ;=============
      ; Local macros
      ;=============

      szText MACRO Name, Text:VARARG
        LOCAL lbl
          jmp lbl
            Name db Text,0
          lbl:
        ENDM

      m2m MACRO M1, M2
        push M2
        pop  M1
      ENDM

      return MACRO arg
        mov eax, arg
        ret
      ENDM

        ;=================
        ; Local prototypes
        ;=================
        WinMain PROTO :DWORD,:DWORD,:DWORD,:DWORD
        WndProc PROTO :DWORD,:DWORD,:DWORD,:DWORD

    .data
	icon_tray_name	db"ICO1",0
    .code
    
include win_main.asm     


start:
        push NULL
        call GetModuleHandle
        mov hInstance, eax

        call GetCommandLine
        mov CommandLine, eax

        push SW_SHOWDEFAULT
        push CommandLine
        push NULL
        push hInstance
        call WinMain

        push eax
        call ExitProcess

; #########################################################################
WinMain proc hInst     :DWORD,
             hPrevInst :DWORD,
             CmdLine   :DWORD,
             CmdShow   :DWORD
             
             LOCAL msg  :MSG

	push CmdShow
	push CmdLine
	push hPrevInst
	push hInst
	call win_main_create
	

      ;===================================
      ; Loop until PostQuitMessage is sent
      ;===================================

    StartLoop:
      push 0
      push 0
      push NULL
      lea eax, msg
      push eax
      call GetMessage

      cmp eax, 0
      jle ExitLoop

      lea eax, msg
      push eax
      call TranslateMessage

      lea eax, msg
      push eax
      call DispatchMessage

      jmp StartLoop
    ExitLoop:

      mov eax, msg.wParam
      ret

WinMain endp


; ########################################################################

 
end start
